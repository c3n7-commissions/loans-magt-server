<?php

use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Company\CompanyController;
use App\Http\Controllers\Lender\BankController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
// return $request->user();
// });


Route::group(['middleware' => ['auth:sanctum']], function () {
  Route::post('/auth/logout', [UserController::class, 'logout'])->name('auth.logout');
  Route::post('/auth/show', [UserController::class, 'show_email'])->name('auth.show');

  Route::name('users.')->group(function () {
    Route::get('/users/', [UserController::class, 'index'])->name('index');
    Route::get('/users/{user}', [UserController::class, 'show'])->name('show');
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('delete');
  });

  Route::resource('company', CompanyController::class)->except(['create', 'edit', 'destroy']);
  Route::resource('banks', BankController::class)->except(['create', 'edit']);
});

Route::post('/auth/register', [UserController::class, 'store'])->name('auth.register');
Route::post('/auth/login', [UserController::class, 'login'])->name('auth.login');
