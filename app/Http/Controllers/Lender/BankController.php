<?php

namespace App\Http\Controllers\Lender;

use App\Http\Controllers\Controller;
use App\Models\Lender\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $banks = Bank::all();

    return response($banks, 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $rules = [
      'bank_name' => 'required|string',
      'branch' => 'required|string',
      'account_name' => 'required|string',
      'account_number' => 'required|string',
      'currency' => 'required|string',
      'cheque_clear_days' => 'required|numeric|integer',
      'overdraft_interest' => 'required|numeric|min:0|max:100',
      'overdraft_limit_interest' => 'required|numeric|min:0|max:100',
      'overdraft_limit' => 'required|numeric',
    ];

    $fields = $request->validate($rules);
    $bank = Bank::create($fields);

    return response($bank, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Lender\Bank  $bank
   * @return \Illuminate\Http\Response
   */
  public function show(Bank $bank)
  {
    return response($bank, 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Lender\Bank  $bank
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Bank $bank)
  {
    $rules = [
      'bank_name' => 'string',
      'branch' => 'string',
      'account_name' => 'string',
      'account_number' => 'string',
      'currency' => 'string',
      'cheque_clear_days' => 'numeric|integer',
      'overdraft_interest' => 'numeric|min:0|max:100',
      'overdraft_limit_interest' => 'numeric|min:0|max:100',
      'overdraft_limit' => 'numeric',
    ];

    $request->validate($rules);

    if ($request->has('bank_name')) {
      $bank->bank_name = $request->bank_name;
    }

    if ($request->has('branch')) {
      $bank->branch = $request->branch;
    }

    if ($request->has('account_name')) {
      $bank->account_name = $request->account_name;
    }

    if ($request->has('account_number')) {
      $bank->account_number = $request->account_number;
    }

    if ($request->has('currency')) {
      $bank->currency = $request->currency;
    }

    if ($request->has('cheque_clear_days')) {
      $bank->cheque_clear_days = $request->cheque_clear_days;
    }

    if ($request->has('overdraft_interest')) {
      $bank->overdraft_interest = $request->overdraft_interest;
    }

    if ($request->has('overdraft_limit_interest')) {
      $bank->overdraft_limit_interest = $request->overdraft_limit_interest;
    }

    if ($request->has('overdraft_limit')) {
      $bank->overdraft_limit = $request->overdraft_limit;
    }

    if ($bank->isClean()) {
      return response(['message' => 'to update specify different values'], 422);
    }

    $bank->save();

    return response($bank, 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Lender\Bank  $bank
   * @return \Illuminate\Http\Response
   */
  public function destroy(Bank $bank)
  {
    $bank->delete();

    return response($bank, 200);
  }
}
