<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\ApiController;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $companies = Company::all();
    return $this->showAll($companies);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $company_count = Company::all()->count();

    if ($company_count >= 1) {
      return response([
        'message' => 'there already exists a company'
      ], 409);
    }

    $rules = [
      'name' => 'required|string',
      'email' => 'required|string',
      'telephone' => 'required|string',
      'postal_address' => 'required|string',
      'physical_address' => 'required|string',
      'logo' => 'image'
    ];

    $fields = $request->validate($rules);

    $data = [
      'name' => $fields['name'],
      'email' => $fields['email'],
      'telephone' => $fields['telephone'],
      'postal_address' => $fields['postal_address'],
      'physical_address' => $fields['physical_address'],
    ];

    if ($request->has('logo')) {
      $data['logo'] = $request->logo->store('', 'images');
    }

    $company = Company::create($data);

    return $this->showOne($company, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Company\Company  $company
   * @return \Illuminate\Http\Response
   */
  public function show(Company $company)
  {
    return $this->showOne($company, 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Company\Company  $company
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Company $company)
  {
    $rules = [
      'name' => 'string',
      'email' => 'string',
      'telephone' => 'string',
      'postal_address' => 'string',
      'physical_address' => 'string',
      'logo' => 'image'
    ];

    $request->validate($rules);

    if ($request->has('name')) {
      $company->name = $request->name;
    }
    if ($request->has('email')) {
      $company->email = $request->email;
    }
    if ($request->has('telephone')) {
      $company->telephone = $request->telephone;
    }
    if ($request->has('postal_address')) {
      $company->postal_address = $request->postal_address;
    }
    if ($request->has('physical_address')) {
      $company->physical_address = $request->physical_address;
    }
    if ($request->has('logo')) {
      Storage::disk('images')->delete($company->logo);
      $company->logo =  $request->logo->store('', 'images');
    }

    if ($company->isClean()) {
      return response(['message' => 'to update specify different values'], 422);
    }

    $company->save();

    return $this->showOne($company, 200);
  }
}
