<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
    $users = User::all();

    return response($users, 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $rules = [
      'name' => 'required|string',
      'email' => 'required|string|unique:users,email',
      'password' => 'required|string|confirmed'
    ];

    $fields = $request->validate($rules);

    $user = User::create([
      'name' => $fields['name'],
      'email' => $fields['email'],
      'password' => Hash::make($fields['password'])
    ]);

    $token = $user->createToken('authtoken')->plainTextToken;

    $response = [
      'user' => $user,
      'token' => $token
    ];
    return response($response, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    return response()->json($user, 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show_email(Request $request)
  {
    $rules = [
      'email' => 'required|string'
    ];
    $fields = $request->validate($rules);
    $user = User::where('email', $fields['email'])->firstOrFail();

    return response($user, 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $user->delete();

    return response()->json($user, 200);
  }

  public function login(Request $request)
  {
    $rules = [
      'email' => 'required|string',
      'password' => 'required|string',
    ];

    $fields = $request->validate($rules);

    $user = User::where('email', $fields['email'])->first();

    if (!$user || !Hash::check($fields['password'], $user->password)) {
      return response([
        'message' => 'invalid credentials'
      ], 401);
    }

    $token = $user->createToken('authtoken')->plainTextToken;

    $response = [
      'user' => $user,
      'token' => $token
    ];
    return response($response, 200);
  }

  public function logout(Request $request)
  {
    $request->user()->tokens()->delete();

    $message = [
      'message' => 'logged out'
    ];

    return response($message, 200);
  }
}
