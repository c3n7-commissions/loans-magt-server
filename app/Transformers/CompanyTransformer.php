<?php

namespace App\Transformers;

use App\Models\Company\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
    //
  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [
    //
  ];

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Company $company)
  {
    return [
      'id' => (int)$company->id,
      'name' => (string)$company->name,
      'email' => (string)$company->email,
      'telephone' => (string)$company->telephone,
      'postal_address' => (string)$company->postal_address,
      'physical_address' => (string)$company->physical_address,
      'logo' => (string)$company->logo,
      'created_at' => $company->created_at,
      'updated_at' => $company->updated_at,

      'links' => [
        [
          'rel' => 'self',
          'href' => route('company.show', $company->id),
          // 'user' => request()->user()
        ]
      ]
    ];
  }
}
