<?php

namespace App\Models\Lender;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
  use HasFactory, SoftDeletes;

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'bank_name',
    'branch',
    'account_name',
    'account_number',
    'currency',
    'cheque_clear_days',
    'overdraft_interest',
    'overdraft_limit_interest',
    'overdraft_limit'
  ];
}
