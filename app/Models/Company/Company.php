<?php

namespace App\Models\Company;

use App\Transformers\CompanyTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  use HasFactory;

  public $transformer = CompanyTransformer::class;

  protected $fillable = [
    'name',
    'email',
    'telephone',
    'postal_address',
    'physical_address',
    'logo'
  ];
}
