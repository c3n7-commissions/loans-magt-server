<?php

namespace App\Traits;

trait ApiResponse
{
  private function successResponse($data, $code)
  {
    return response()->json($data, $code);
  }

  protected function errorResponse($data, $code)
  {
    return response()->json(["message" => $data], $code);
  }

  protected function showAll($collection, $code = 200)
  {
    if ($collection->isEmpty()) {
      return $this->successResponse(["data" => $collection], $code);
    }
    $transformer = $collection->first()->transformer;
    $collection = fractal($collection, new $transformer)->toArray();

    return $this->successResponse($collection, $code);
  }


  protected function showOne($instance, $code = 200)
  {
    $transformer = $instance->transformer;
    $instance = fractal($instance, new $transformer);

    return $this->successResponse($instance, $code);
  }
}
