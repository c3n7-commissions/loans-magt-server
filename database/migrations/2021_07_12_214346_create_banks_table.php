<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('banks', function (Blueprint $table) {
      $table->id();
      $table->string('bank_name');
      $table->string('branch');
      $table->string('account_name');
      $table->string('account_number');
      $table->string('currency');
      $table->unsignedSmallInteger('cheque_clear_days');
      $table->float('overdraft_interest');
      $table->float('overdraft_limit_interest');
      $table->double('overdraft_limit');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('banks');
  }
}
